#!/bin/sh

cp -Ru /default/* /DATA
chown -Rf nginx:nginx /DATA/public_html

# start nginx
mkdir -p /tmp/nginx
chown nginx:nginx /tmp/nginx
chown -Rf nginx:nginx /DATA/logs/
rm -f /etc/php7/php.ini
ln -s /DATA/config/php.ini /etc/php7/php.ini
rm -f /etc/php7/php-fpm.conf
ln -s /DATA/config/php-fpm.conf /etc/php7/php-fpm.conf
rm -f /etc/nginx/nginx.conf
ln -s /DATA/config/nginx.conf /etc/nginx/nginx.conf

if [ -f $WORDPRESS_DIR/wp-config.php ]
then
echo 'No need to edit the config file... starting php-fpm'
else
echo 'Editing wp-config.php...'
cp /DATA/public_html/wp-config-sample.php /DATA/public_html/wp-config.php
sed -i "s/database_name_here/$DB_NAME/" /DATA/public_html/wp-config.php
sed -i "s/username_here/$DB_USER/" /DATA/public_html/wp-config.php
sed -i "s/password_here/$DB_PASS/" /DATA/public_html/wp-config.php
sed -i "s/localhost/172.17.0.1/" /DATA/public_html/wp-config.php
fi

php-fpm7
nginx