FROM nginx-php-fpm:latest
MAINTAINER Nguyen Cap Tien. <nguyen@crystal-techs.com>

RUN set -ex; \
	curl -o wordpress.tar.gz -fSL "https://wordpress.org/latest.tar.gz"; \
	tar -xzf wordpress.tar.gz -C /default/; \	
    rm wordpress.tar.gz;\
    cp -Ru /default/wordpress/* /default/public_html/;\
    rm -R /default/wordpress;

ADD run.sh /
RUN chmod +x /run.sh

CMD ["/run.sh"]